# Zucchini.tmbundle

A **TextMate Bundle** for the **Zucchini** functional testing framework.

The bundle includes syntax highlighting, the ability to run feature walkthroughs and reports and a number of useful snippets.

![screenshot](http://zucchiniframework.org/i/zucchini-textmate.png)

## Sublime Text

The bundle is available through [Package Control](https://sublime.wbond.net/).

## TextMate

### Installation

For version **2.0**

```
mkdir -p ~/Library/Application\ Support/Avian/Bundles
cd ~/Library/Application\ Support/Avian/Bundles
git clone git@github.com:zucchini-src/zucchini.tmbundle.git Zucchini.tmbundle
```

For **1.5.x**

```
mkdir -p ~/Library/Application\ Support/TextMate/Bundles
cd ~/Library/Application\ Support/TextMate/Bundles
git clone git@github.com:zucchini-src/zucchini.tmbundle.git Zucchini.tmbundle
```

### Running features on a device

If your TextMate.app is having trouble finding the `zucchini` command, remember that [TextMate doesn't inherit your regular PATH](http://wiki.macromates.com/Troubleshooting/TextMateAndThePath). The bundle also executes a `.rvmrc` it finds in the project directory.

Patches and additions are welcome.
